/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ArrayDeletionsLab;

import java.util.Scanner;

/**
 *
 * @author Nobpharat
 */
public class RemoveDuplicate {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the length of the array: ");
        int length = scanner.nextInt();
        int[] nums = new int[length];

        System.out.print("Enter the elements of the array:");
        for (int i = 0; i < length; i++) {
            nums[i] = scanner.nextInt();
        }

        int k = removeDuplicates(nums);

        System.out.print("Output: " + k + ", nums = ");
        printArray(nums, k);
    }

    private static int removeDuplicates(int[] nums) {
        int k = 1; 
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[k] = nums[i];
                k++;
            }
        }

        return k;
    }

    private static void printArray(int[] arr, int length) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            if (i < length) {
                System.out.print(arr[i]);
            } else {
                System.out.print("_");
            }
            if (i < arr.length - 1) {
                System.out.print(" , ");
            }
        }
        System.out.print("]");
        System.out.println();
    }
}
