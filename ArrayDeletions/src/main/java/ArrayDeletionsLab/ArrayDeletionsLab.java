/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ArrayDeletionsLab;

/**
 *
 * @author informatics
 */
public class ArrayDeletionsLab {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        int index = 2;
        int value = 4;

        System.out.print("Original Array: ");
        printArray(arr);

        int[] newArrByIndex = deleteElementByIndex(arr, index);

        System.out.print("Array after deleting element at index " + index + ": ");
        printArray(newArrByIndex);

        int[] newArrByValue = deleteElementByValue(arr, value);

        System.out.print("Array after deleting element with value " + value + ": ");
        printArray(newArrByValue);
        
    }

    private static int[] deleteElementByIndex(int[] arr, int index) {

        int[] newArr = new int[arr.length - 1];
        int newIndex = 0;

        for (int i = 0; i < arr.length; i++) {
            if (i != index) {
                newArr[newIndex] = arr[i];
                newIndex++;
            }
        }

        return newArr;
    }

    private static int[] deleteElementByValue(int[] arr, int value) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                count++;
            }
        }

        int[] newArr = new int[arr.length - count];
        int newIndex = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != value) {
                newArr[newIndex] = arr[i];
                newIndex++;
            }
        }

        return newArr;
    }

    private static void printArray(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
            if (i < arr.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.print("]");
        System.out.println();
    }
}

    

